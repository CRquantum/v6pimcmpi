

\begin{enumerate}

\item The computational aspects of the PIMC method in Sec. III are discussed in great detail. I recommend the authors shorten Sec. III (in particular Secs. III.B and III.C), and move some of the more technical parts from the main text to the supplemental material.
}    

We have shortened those sections and moved the details of the calculations
to appendices.

\item The GFMC results for the ground-state energy of He4 agree within error bars with the PIMC results (Fig. 2) when using the AV6’
interaction but not for the chiral N$^2$LO interaction (Fig. 3). Is there an explanation for this?
}

Although the GFMC ground state energy of $^4$He for the N$^2$LO interaction
from paper ``J. E. Lynn, et al., Physical Review Letters 113, 192501
(2014)'' which is 24.86(1) MeV is outside our PIMC error bars 24.99(1)
MeV, the agreement with the Argonne interaction where the Hamiltonian
is well described makes us confident that our path integral result is
correct for our given Hamiltonian.

The fact that the results of Lynn et al. and our PIMC results differ by less than $1\%$, indicates that our results are still consistent with each other. 
However,
Lynn et al., being a letter, does not give complete details of their
calculation.
We suspect that the Hamiltonian used in their paper may be somewhat
different --- perhaps in the treatment of the electromagnetic interactions.
Given this uncertainty, we prefer not to speculate on the origin. We
are confident our result is correct for our Hamiltonian.


\item It would be useful to discuss the scaling of the required computational time with the number of nucleons and the number of time slices.

We have added this discussion at the end of our results section.

\item The authors present their results as a benchmark for PIMC calculations in larger nuclei (see, e.g., in their conclusion).
However, for A > 4 (i.e., beyond s-wave nuclei) one expects to have a sign problem and their conclusion should include a comment about this limitation.
}

We have added a discussion of the sign problem and the approximations
needed to address it for larger nuclei to our conclusion.

